# check if string is palindrome

import sys

def is_palindrome(string):
    revstring = string[::-1]
    if string == revstring:
        return True
    else:
        return False
print("Given string is", sys.argv[1])
print("Is Palindrome: ", is_palindrome(sys.argv[1]))
