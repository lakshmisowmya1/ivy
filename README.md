CheckIFPalindrome

**PULL IMAGE:**

Option 1. Container Registry 

      docker pull registry.gitlab.com/lakshmisowmya1/ivy 

Option 2. Artifact 

      a. Download artifact 
      b. docker load < docker-image.tar  # To load an image from artifact downloaded 

**RUN CONTAINER**

docker run registry.gitlab.com/lakshmisowmya1/ivy  [StringToCheckIFPalindrome]

**Sample Ouputs**

`lgunupudi$ docker run registry.gitlab.com/lakshmisowmya1/ivy level
Given string is level
Is Palindrome:  True`

`lgunupudi$ docker run registry.gitlab.com/lakshmisowmya1/ivy levels
Given string is levels
Is Palindrome:  False`
